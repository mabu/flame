#include <SDL2/SDL.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include "functions.h"
#include "params.h"
#include "types.h"

/*
 * Timer callback: just generates a SDL_USEREVENT to be
 * receipt of the event loop.
 */
unsigned int TimerCallback(unsigned int interval, void *param)
{
    SDL_Event event;
    SDL_UserEvent userevent = {0};

    param = param;

    userevent.type = SDL_USEREVENT;

    event.type = SDL_USEREVENT;
    event.user = userevent;

    SDL_PushEvent(&event);

    return interval;
}

struct context
{
    float *data[3];
    int w, h, s;
    flame_config_t *cfg;
} cn = {{NULL, NULL, NULL}, 0, 0, 0, NULL};

void init(int w, int h)
{
    cn.w = cn.s = w;
    cn.h = h;
    if (h>w)
        { cn.s = h;}

    if (cn.cfg)
        { free(cn.cfg); }

    if (!*cn.data)
    {
        int i;
        for (i = 0; i < 3; ++i)
        {
            cn.data[i] = calloc(cn.s*cn.s, sizeof **cn.data);
        }
    }
    else
    {
        int i;
        for (i = 0; i < 3; ++i)
        {
            memset(cn.data[i], 0, cn.s*cn.s * sizeof **cn.data);
        }
    }

    params_t par = {0};

    cn.cfg = CFG_get_config(&par);
}

void animate(unsigned char *img)
{
    FLAME_generate(cn.data, cn.s, 10000, cn.cfg);
    FLAME_render(img, cn.data, cn.w, cn.h, cn.s);
}

void usage(char *name)
{
    printf("usage:\t%s [-h HEIGHT] [-w WIDTH]\n", name);
}

int main(int argc, char *argv[])
{
    SDL_Window *window = NULL;
    SDL_Surface *screen = NULL;
    int w = 600, h = 400;
    int d = 20;

    int opt;

    while ((opt = getopt(argc, argv, "w:h:d:"))!= -1)
    {
        switch (opt)
        {
        case 'd':
            d = strtoul(optarg, NULL, 0);
            break;
        case 'w':
            w = strtoul(optarg, NULL, 0);
            break;
        case 'h':
            h = strtoul(optarg, NULL, 0);
            break;
        default:
            usage(*argv);
            exit(EXIT_FAILURE);
        }
    }

    // SDL_Init 0 on success and returns negative on failure
    if (SDL_Init(SDL_INIT_EVERYTHING) )
    {
        SDL_Log("Couldn't initialize SDL: %s", SDL_GetError());
        exit (EXIT_FAILURE);
    }
    // creating Window and Surface
    window =
    SDL_CreateWindow(__FILE__, SDL_WINDOWPOS_UNDEFINED,
             SDL_WINDOWPOS_UNDEFINED, w, h,
             SDL_WINDOW_SHOWN);

    // SDL_CreateWindow returns window on creation and NULL on failure
    if ( NULL == window)
    {
        SDL_Log("Couldn't create window: %s", SDL_GetError());
        exit (EXIT_FAILURE);
    }

    // get window surface
    screen = SDL_GetWindowSurface(window);
    if (NULL == screen)
    {
        SDL_Log("Couldn't get window surface: %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    srand(time(NULL));
    init(w, h);

    int running = 1;
    SDL_Event event;

    SDL_AddTimer(100, TimerCallback, NULL);

    time_t next_init = time(NULL) + d;

    while (running)
    {
        int draw = 0;
        while (SDL_PollEvent(&event))
        {

            switch (event.type)
            {
            case SDL_QUIT:
                running = 0;
                break;
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case 'q':
                case 0x1b:
                    running = 0;
                    break;
                case 'n':
                    draw = 1;
                    next_init = 0;
                    break;
                }
                break;
            case SDL_USEREVENT:
                draw = 1;
               break;
            }
            //SDL_UpdateWindowSurface(window);
        }
        if (draw)
        {
            if (time(NULL) > next_init)
            {
                init(w, h);
                next_init = time(NULL) + d;
            }
            else
            {
                animate(screen->pixels);
            }
            SDL_UpdateWindowSurface(window);
        }
    }

    // deallocate surface
    SDL_FreeSurface(screen);
    // destroy window
    SDL_DestroyWindow(window);
    // SDL_Quit();
    SDL_Quit();

    return 0;
}
