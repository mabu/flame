#ifndef H_20180801_OPTIONS
#define H_20180801_OPTIONS

/* parameters of program */
typedef struct params
{
    /* file to generate (.bmp)*/
    char *filename;
    /* file to read parameters from */
    char *input;
    /* file to write parameters into */
    char *output;
    /* image width, in pixels */
    int width;
    /* image height, in pixels */
    int height;
    /* number of functions to generate */
    int nb_functions;
    /* number of points to compute (to to draw) */
    int nb_points;
    /* random generator seed */
    int seed;
    /* verbose mode */
    int verbose;
    /* tolerance for linear transformation */
    float tolerance;
    /* other flags */
    int flags;
}
params_t;

#define PARAM_FLAG_NO_POST_TRANSFORM 1
#define PARAM_FLAG_NO_FINAL_TRANSFORM 2

#define DEFAULT_PARAMS { NULL, NULL, NULL, 256, 256, 0, 5000000, 0, 0, .01, 0} 


#endif 
