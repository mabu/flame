#define _USE_MATH_DEFINES
#include <math.h>
#include "variations.h"
#include "functions.h"

/* coefficients for dependent variations 
 * I hope it will evolve not too late... */
static float a() { return 1; }
static float b() { return 1; }
static float c() { return 1; }
static float d() { return 1; }
static float e() { return 1; }
static float f() { return 1; }

static float mod(float dividend, float divisor)
{
    return dividend - divisor * floorf(dividend / divisor);
}

/* linear */
pointf_t var00(pointf_t pt)
{
    return pt;
}

/* sinusoidal */
pointf_t var01(pointf_t pt)
{
    pt.x = sinf(pt.x);
    pt.y = sinf(pt.y);
    return pt;
}

/* spherical */
pointf_t var02(pointf_t pt)
{
    float r2 = pt.x * pt.x + pt.y * pt.y;
    pt.x /= r2;
    pt.y /= r2;
    return pt;
}

/* swirl */
pointf_t var03(pointf_t pt)
{
    pointf_t ret;
    float r2 = pt.x * pt.x + pt.y * pt.y;
    float sinr2 = sinf(r2);
    float cosr2 = cosf(r2);
    ret.x = pt.x * sinr2 - pt.y * cosr2;
    ret.y = pt.x * cosr2 + pt.y * sinr2;
    return ret;
}

/* horseshoe */
pointf_t var04(pointf_t pt)
{
    pointf_t ret;
    float one_on_r = 1.f/sqrtf(pt.x * pt.x + pt.y * pt.y);
    ret.x = one_on_r * (pt.x - pt.y)*(pt.x + pt.y);
    ret.y = one_on_r * (2.f * pt.x * pt.y);
    return ret;
}

/* polar */
pointf_t var05(pointf_t pt)
{
    pointf_t ret;
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    /* multiply by inv(pi) instead of dividing... useless optimization? */
    ret.x = Theta * (float)M_1_PI;
    ret.y = r - 1.f;
    return ret;
}

/* Handkerchief */
pointf_t var06(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    pt.x = r * sinf(Theta + r);
    pt.y = r * cosf(Theta - r);
    return pt;
}

/* Heart */
pointf_t var07(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    pt.x = r * sinf(Theta * r);
    pt.y = -r * cosf(Theta * r);
    return pt;
}

/* Disc */
pointf_t var08(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    pt.x = Theta * (float)M_1_PI * sinf(Theta * r);
    pt.y = Theta * (float)M_1_PI * cosf(Theta * r);
    return pt;
}

/* Spiral */
pointf_t var09(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    pt.x = (cosf(Theta) + sinf(r))/r;
    pt.y = (sinf(Theta) - cosf(r))/r;
    return pt;
}

/* Hyperbolic */
pointf_t var10(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    pt.x = sinf(Theta) / r;
    pt.y = r * cosf(Theta);
    return pt;
}

/* Diamond */
pointf_t var11(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    pt.x = sinf(Theta) * cosf(r);
    pt.y = cosf(Theta) * sinf(r);
    return pt;
}

/* Ex */
pointf_t var12(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    float p0 = sinf(Theta + r);
    float p1 = cosf(Theta - r);
    p0 = p0*p0*p0;
    p1 = p1*p1*p1;
    pt.x = r*(p0+p1);
    pt.y = r*(p0-p1);
    return pt;
}

/* Julia */
pointf_t var13(pointf_t pt)
{  
    float Omega = TOOLS_randi(0, 1) ? 0 : M_PI;
    float sqr = sqrtf(sqrtf(pt.x * pt.x + pt.y * pt.y));
    float Theta = atan2f(pt.x, pt.y);

    pt.x = sqr * cosf(Theta * .5f + Omega);
    pt.y = sqr * sinf(Theta * .5f + Omega);
    return pt;
}

/* Bent */
pointf_t var14(pointf_t pt)
{
    if (pt.x < 0)
    {
        pt.x += pt.x;
    }
    if (pt.y < 0)
    {
        pt.y *= .5f;
    }
    return pt;
}

/* Waves */
pointf_t var15(pointf_t pt)
{
    pointf_t ret;
    ret.x = pt.x + b()*sinf(pt.y/c()/c());
    ret.y = pt.y + e()*sinf(pt.x/f()/f());
    return ret;
}

/* Fisheye */
pointf_t var16(pointf_t pt)
{
    pointf_t ret;
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float factor = 2.f / (r + 1.f);
    ret.x = pt.y * factor;
    ret.y = pt.x * factor;
    return ret;
}

/* Popcorn */
pointf_t var17(pointf_t pt)
{
    pointf_t ret;
    ret.x = pt.x + c()*sinf(tanf(3.f * pt.y));
    ret.y = pt.y + f()*sinf(tanf(3.f * pt.x));
    return ret;
}

/* Exponential */
pointf_t var18(pointf_t pt)
{    
    /*
    float expv = expf(pt.x -1.f);
    pt.x = expv * cosf(M_PI * pt.y);
    pt.y = expv * sinf(M_PI * pt.y);
    */
    return pt;
}

/* Power */
pointf_t var19(pointf_t pt)
{
    float Theta = atan2f(pt.x, pt.y);
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    r = powf(r, sinf(Theta));
    pt.x = r * cosf(Theta);
    pt.y = r * sinf(Theta);
    return pt;
}

/* Cosine */
pointf_t var20(pointf_t pt)
{
    /*
    pointf_t ret;
    ret.x = cosf(M_PI * pt.x) * coshf(pt.y);
    ret.y = sinf(M_PI * pt.x) * sinhf(pt.y);
    return ret;
    */
    return pt;
}
/* Rings */
pointf_t var21(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    float Theta = atan2f(pt.x, pt.y);
    float rho, c2 = c()*c();

    rho = mod(r + c2, 2.f*c2) - c2 + r * (1-c2);
    pt.x = rho * cosf(Theta);
    pt.y = rho * sinf(Theta);
    return pt;
}

/* Fan */
pointf_t var22(pointf_t pt)
{
    float t = M_PI * c()*c();
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);    
    float Theta = atan2f(pt.x, pt.y);
    float rho;

    rho = mod(Theta + f(), t);
    t /= 2.f;

    if (rho > t)
    {
        pt.x = r * cosf(Theta - t);
        pt.y = r * sinf(Theta - t);
    }    
    else
    {
        pt.x = r * cosf(Theta + t);
        pt.y = r * sinf(Theta + t);
    }
    return pt;
}
 
/* Eyefish */
pointf_t var27(pointf_t pt)
{
    float r = sqrtf(pt.x * pt.x + pt.y * pt.y);
    r = 2.f / (r + 1.f);
    pt.x *= r;
    pt.y *= r;
    return pt;
}

/* Bubble */
pointf_t var28(pointf_t pt)
{
    float r2 = pt.x * pt.x + pt.y * pt.y;
    r2 = 4.f / (r2 + 4.f);
    pt.x *= r2;
    pt.y *= r2;
    return pt;
}

/* Cylinder */
pointf_t var29(pointf_t pt)
{
    pt.x = sinf(pt.x);
    return pt;
}

/* Noise */
pointf_t var31(pointf_t pt)
{
    float Psi_1 = TOOLS_randf(0, 1);
    float Psi_2 = TOOLS_randf(0, 2);
    pt.x = Psi_1 * pt.x * cosf(M_PI * Psi_2);
    pt.y = Psi_1 * pt.x * sinf(M_PI * Psi_2);
    return pt;
}

/* Blur */
pointf_t var34(pointf_t pt)
{
    float Psi_1 = TOOLS_randf(0, 1);
    float Psi_2 = TOOLS_randf(0, 2);
    pt.x = Psi_1 * cosf(M_PI * Psi_2);
    pt.y = Psi_1 * sinf(M_PI * Psi_2);
    return pt;
}

variation_t s_variations [] = 
{ 
    var00,  var01,  var02,  var03,  var04,
    var05,  var06,  var07,  var08,  var09,
    var10,  var11,  var12,  var13,  var14,
    var15,  var16,  var17,  var18,  var19, 
    var20,  var21,  var22,   
                    var27,  var28,  var29,
            var31,                  var34
    
};

variation_t *variations = s_variations;

int variations_cnt = sizeof s_variations / sizeof *s_variations;


int VAR_get_variation_address(void* addr)
{
    int test;
    int ret = -1;

    for (test = 0; test < variations_cnt; ++test)
    {
        if (addr == s_variations[test])
        {
            ret = test;
            break;
        }
    }

    return ret;
}

