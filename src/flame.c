#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifdef _OPENMP
#include <omp.h>
#endif

#include "params.h"
#include "functions.h"
#include "variations.h"

#ifdef _WIN32
#define finite _finite
#endif

#define CLAMP(x, m, M) if ((x) > (M)) {(x) = (M);} else if ((x) < (m)) {(x) = (m);}


/**
 * Transform one point from graph [-1,+1]^2 înto [0,side]^2
 */
point_t grath_to_pixel(pointf_t ptf, int side)
{
    point_t ret;
    ret.x = (int)((ptf.x + 1.f)*side*.5f);
    ret.y = (int)((ptf.y + 1.f)*side*.5f);
    return ret;
}

pointf_t function(pointf_t pt, funpars_t param)
{
    pointf_t ret;
    /* first affine transformation */
    ret.x = pt.x * param.par[0] + pt.y * param.par[1] + param.par[2];
    ret.y = pt.x * param.par[3] + pt.y * param.par[4] + param.par[5];
    /* non linear variation */
    pt = param.var(ret);
    /* post transformation */
    ret.x = pt.x * param.post[0] + pt.y * param.post[1] + param.post[2];
    ret.y = pt.x * param.post[3] + pt.y * param.post[4] + param.post[5];
    return ret;

}

colorf_t function_color(colorf_t col, funpars_t param)
{
    int i;
    colorf_t ret;
    for (i = 0; i < 3; ++i)
    {
        ret.comp[i] = .5f*(col.comp[i] + param.color.comp[i]);
        //ret.comp[i] = .99f*col.comp[i] + .01f*param.color.comp[i];
    }

    return ret;
}

void apply_symetry(float **data, int nb_comp, int side, int kind)
{
    int i, x, y, k;
    int half = side / 2;
    float *sum;
    int cnt = side*side;
    int size = side * side * sizeof *sum;

    if (0 == kind)
    {
        /* if no symetry, return now */
        return;
    }

    sum = malloc(size);
    for (i = 0; i < nb_comp; ++i)
    {
        memcpy(sum, data[i], size);
        switch (kind)
        {
            /* vertical symetry*/
            case 1:
                for (k = y = 0; y < side; ++y)
                {
                    int offset = (y + 1) * side;
                    for (x = 0; x < side; ++x, ++k)
                    {
                        sum[k] += data[i][offset - x];
                        sum[k] *= .5f;
                    }
                }
                break;
            /* horizontal and vertical symetry*/
            case 2:
                for (k = y = 0; y < side; ++y)
                {
                    int offset = (y + 1) * side;
                    int offset_2 = (side - 1 - y) * side;

                    for (x = 0; x < side; ++x, ++k)
                    {
                        sum[k] += data[i][offset - x];
                        sum[k] += data[i][offset_2 + x];
                        sum[k] += data[i][offset_2 + side - x];
                        sum[k] *= .25f;
                    }
                }
                break;
            /* 180 deg rotation */
            case 3:
                for (k = y = 0; y < side; ++y)
                {
                    int offset = side * (side - y) - 1;
                    for (x = 0; x < side; ++x, ++k)
                    {
                        sum[k] += data[i][offset - x];
                        sum[k] *= .5f;
                    }
                }
                break;
            /* 120 deg rotation */
            /**
             * @todo at least compute a bilinear rotation...
             */
            case 4:
                for (k = y = 0; y < side; ++y)
                {
                    #define COS -.5f
                    #define SIN 0.8660254f
                    int dy = y - half;
                    for (x = 0; x < side; ++x, ++k)
                    {
                        int dx = x - half, dX, dY, idx;

                        dX = (int)(COS * dx - SIN * dy);
                        dY = (int)(SIN * dx + COS * dy);

                        idx = side * (half + dY) + half + dX;
                        if (idx > 0 && idx < cnt)
                            { sum[k] += data[i][idx]; }
                        dX = (int)(COS * dx + SIN * dy);
                        dY = (int)(-SIN * dx + COS * dy);

                        idx = side * (half + dY) + half + dX;
                        if (idx > 0 && idx < cnt)
                            { sum[k] += data[i][idx]; }

                        sum[k] *= .333f;
                    }
                    #undef COS
                    #undef SIN
                }
                break;
            /* 90 deg rotation */
            case 5:
                for (k = y = 0; y < side; ++y)
                {
                    for (x = 0; x < side; ++x, ++k)
                    {
                        sum[k] += data[i][side * (side - y) - x - 1];
                        sum[k] += data[i][side * x + side - y - 1];
                        sum[k] += data[i][side * (side - x - 1) + y ];
                        sum[k] *= .25f;
                    }
                }
                break;
            /* y=x symetry 180 deg rotation */
            case 6:
            for (k = y = 0; y < side; ++y)
                {
                    for (x = 0; x < side; ++x, ++k)
                    {
                        sum[k] += data[i][side * x + y];
                        sum[k] *= .5f;
                    }
                }
                break;

        }
        memcpy(data[i], sum, size);
    }
    free(sum);
}

void FLAME_generate(float *data[3], int side, int nb_pts, flame_config_t *cfg)
{
    int size = side * side;
    float limit = 10.f*side;
    long long unsigned int pt_cnt = 0;

    #pragma omp parallel shared(data, pt_cnt)
    {
        long long unsigned int cnt = 0, i, j;
        int nb_points = nb_pts;
        /* first point for iterations */
        pointf_t ptf = {0.f, 0.f};
        colorf_t col = {{1.f, 1.f, 1.f}};


        #ifdef _OPENMP
        nb_points /= omp_get_num_threads();
        #endif
        /* lets compute 20 points without remembering them */
    RESTART:
        ptf.x = ptf.y = 0;
        
        for (i = 0; i < 20; ++i)
        {
            int p = TOOLS_randi(0, cfg->nb_funcs-1);
            ptf = function(ptf, cfg->funcs[p]);
            col = function_color(col, cfg->funcs[p]);
        }

        for (; cnt < nb_points; ++cnt)
        {
            int p, idx;
            pointf_t pt_final;
            colorf_t col_final;
            point_t pt;

            /* iteration */
            p = TOOLS_randi(0, cfg->nb_funcs-1);
            ptf = function(ptf, cfg->funcs[p]);
            col = function_color(col, cfg->funcs[p]);


            if (!finite(ptf.x) || !finite(ptf.y) || 
                ptf.x > limit || ptf.x < -limit || 
                ptf.y > limit || ptf.y < -limit)
            {
                goto RESTART;
            }


            /* final transfrom */
            pt_final = function(ptf, cfg->final);
            col_final = function_color(col, cfg->final);
            pt = grath_to_pixel(pt_final, side);

            if (pt.x >= 0 && pt.x < side &&
                pt.y >= 0 && pt.y < side)
            {
            
                /* compute pixel position in image array */
                idx = pt.x + pt.y * side;

                #pragma omp critical
                {
                    pt_cnt++;

                    for (j = 0; j < 3; ++j)
                        data[j][idx] = 
                            .99f * data[j][idx] + 
                            .01f * col_final.comp[j];
                
                }
            }
        }
    } /* end of omp parallel */

    apply_symetry(data, 3, side, cfg->symetry);
}

float ** FLAME_resize(float *fractal[3], int side, int new_side)
{
    float **tmp;
    int x, xx, y, yy;
    int reduction = side / new_side;
    float factor = 1.f/(float)(reduction*reduction);
    if ((reduction < 2)||((new_side*reduction) != side))
    {
        fprintf(stderr, "bad reduction factor %d --> %d\n", side, new_side);
        return NULL;
    }

    tmp = malloc(3*sizeof *tmp);
    for (x = 0; x < 3; ++x)
    {
        tmp[x] = calloc(new_side*new_side, sizeof **tmp);
    }

    for (y = 0; y < new_side; ++y)
    {
        for (x = 0; x < new_side; ++x)
        {
            int idx = y * new_side + x;
            for (yy = 0; yy < reduction; ++yy)
            {
                int offset = (y * reduction + yy) * side + x * reduction;
                for (xx = 0; xx < reduction; ++xx)
                {
                    tmp[0][idx] += fractal[0][offset + xx];
                    tmp[1][idx] += fractal[1][offset + xx];
                    tmp[2][idx] += fractal[2][offset + xx];
                }
            }
            tmp[0][idx] *= factor;
            tmp[1][idx] *= factor;
            tmp[2][idx] *= factor;
        }
    }
    return tmp;
}

static void get_last_centil(float *fractal[3], float cen[3], size_t side)
{
    const size_t histo_size = 2047;
    size_t size = (histo_size + 1) * sizeof (float);
    int i;
    int *histo = malloc(size);
    if (!histo)
    {
        //perror("malloc");
        return;
    }

    for (i = 0; i < 3; ++i)
    {
        size_t j, centil = 99*side*side/100;
        float max = 0;
        float *p = fractal[i], *end = p + side*side;

        /* find max */
        while (p != end)
        {
            if (max < *p)
                { max = *p; }

            ++p;
        }
        //printf("\nfor comp %i, max is %f\n", i, max);
        max *= 1.1f;
        /* compute histo */
        memset(histo, 0, size);
        p = fractal[i];
        while (p != end)
        {
            size_t idx = (size_t)(*p / max * (float)histo_size);
            histo[idx]++;
            p++;
        }


        /* cumul */
        for (j = 0; j < histo_size; ++j)
        {
            histo[j+1] += histo[j];
        }
        //printf("sum is %ld / %ld\n", histo[histo_size], side*side);
        for (j = histo_size; j > 0; --j)
        {
            if (histo[j] < centil)
            {
                cen[i] = (float)j / (float)histo_size * max;
                break;
            }
        }
        //printf("last centil[%d] is %g\n", i, cen[i]);
    }
    free(histo);
}

void FLAME_render(unsigned char *image, float *fractal[3], int width, int height, int side)
{
    int x, y, j, k;
    float ratio;
    int decal_x = (side - width)/2;
    int decal_y = (side - height)/2;
    int end_x = side - decal_x;
    int end_y = side - decal_y;
    float cen[3];

    get_last_centil(fractal, cen, side);
    ratio = cen[0];
    for (j = 1; j < 3; ++j)
    {
        if (ratio < cen[j])
            ratio = cen[j];
    }

    /* prepare log-density display and clamp and copy to image */
    for (k = 0, y = decal_y; y < end_y; ++y)
    {
        int offset = side * y;
        for (x = decal_x; x < end_x; ++x)
        {
            for (j = 0; j < 3; ++j, ++k)
            {
                /* log density */
                //float v = 500.f*log10f(1.f + fractal[j][offset + x] * ratio);
                //float v = 300.f * log10f(/*1.f +*/ 7.f * fractal[j][offset + x]/ratio);
                float v = 7.f * fractal[j][offset + x]/ratio;
                if (v>1)
                    v = 300.f*log10f(v);
                else
                    v = 0;

                /* clamp */
                CLAMP(v, 0, 255);

                /* copy */
                image[k] = (unsigned char) v;
            }

            /* ignore the image fourth component */
            ++k;
        }
    }
}
