#include <stdlib.h>

/* get a random number in [min, max]*/
int TOOLS_randi(int min, int max)
{
    double r = (double) rand() / ((double) RAND_MAX+1.);
    r *= (double)(max + 1 - min);
    r += (double)min;
    return (int) r;
}

/* get a random number in [min, max]*/
float TOOLS_randf(float min, float max)
{
    float r = (float) rand() / (float) RAND_MAX;
    r *= (max - min);
    r += min;
    return r;
}

