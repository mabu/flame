#ifndef H_20180116_TYPES
#define H_20180116_TYPES

/* mathematical point (almost used in [-1, 1]**2 */
typedef struct pointf
{
    float x;
    float y;
} pointf_t;

typedef pointf_t (*variation_t) (pointf_t);


/* screen point [0, w-1]x[0, h-1]*/
typedef struct point
{
    int x;
    int y;
}
point_t;

/* rgb color, in [0,1]^3 */
typedef struct colorf
{
    float comp[3];
}
colorf_t;



/* parameters for one function in the 
 * function system */
typedef struct function_params
{
    /* first transformation */
    float par[6];
    /* post transformation */
    float post[6];
    /* function color */
    colorf_t color;
    /* non linear variation */
    variation_t var;
} funpars_t;

/**
 * The flame_config structure only configure
 * the fractal drawing, not the image writting,
 * so dimension, number of points to draw are not
 * here.
 */
typedef struct flame_config 
{
    /* number of functions */
    int nb_funcs;
    /* symetry mode */
    int symetry;
    /* final transformation */
    funpars_t final;
    /* list of transformations */
    funpars_t *funcs;
} flame_config_t;



#endif /* guard */
