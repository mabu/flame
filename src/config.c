#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "functions.h"
#include "params.h"
#include "types.h"
#include "variations.h"

/* evaluate a function, ignoring the non linear variation */
pointf_t eval_function(pointf_t pt, funpars_t param)
{
    pointf_t tmp;
    /* first affine transformation */
    tmp.x = pt.x * param.par[0] + pt.y * param.par[1] + param.par[2];
    tmp.y = pt.x * param.par[3] + pt.y * param.par[4] + param.par[5];

    /* post transformation */
    pt.x = tmp.x * param.post[0] + tmp.y * param.post[1] + param.post[2];
    pt.y = tmp.x * param.post[3] + tmp.y * param.post[4] + param.post[5];

    return pt;
}
funpars_t * generate_random_functions(int *nb_functions, float tolerance, int disable_post)
{
    int fun;
    funpars_t *ret = NULL;

    /* generate number of functions, at least 3 */
    if (*nb_functions < 3)
    {
        *nb_functions = TOOLS_randi(3, 100);
    }

    /* allocate N functions + the final function */
    ret = calloc(1 + *nb_functions, sizeof *ret);

    /* generate functions */
    for (fun = 0; fun <= *nb_functions; ++fun)
    {
        int i, ign, par;
        const float range = fabsf(tolerance);
        const float par_def[] = {1, 0, 0, 0, 1, 0};
        /* set transformation parameters */
        for (par = 0; par < 6; ++par)
        {
            ret[fun].par[par] = par_def[par] + TOOLS_randf(-range, range);
        }

        /* set post transformation parameters */
        if (disable_post)
        {
            ret[fun].post[0] = ret[fun].post[4] = 1;
        }
        else
        {
            for (par = 0; par < 6; ++par)
            {
                ret[fun].post[par] = par_def[par] + TOOLS_randf(-range, range);
            }
        }

        /* select variation */
        i = TOOLS_randi(0, variations_cnt-1);
        ret[fun].var = variations[i];

        /** generate color */
        /* composant to ignore */
        ign = TOOLS_randi(0, 3);

        /* generate random color */
        for (i = 0; i < 3; ++i)
        {
            if (ign == i) { continue; }
            ret[fun].color.comp[i] = TOOLS_randf(.5f, 1.f);
        }
    }

    return ret;
}

float * generate_v_matrix(int nb_func)
{
    int i, j; 
    /* generate v_i,j matrix */

    float *v = malloc(nb_func * nb_func * sizeof *v);

    /* todo: add malloc return test...*/

    for (i = 0; i < nb_func; ++i)
    {
        float sum = 0;
        for (j = 0; j < nb_func; ++j)
        {
            float val = (double) rand();
            v[j * nb_func + i] = val;
            sum += val;
        }
        for (j = 0; j < nb_func; ++j)
        {
            v[j * nb_func + i] /= sum;
        }
    }
    return v; 
}



int read_config_from_file(flame_config_t *flm, const char *filename)
{
    int ret = 0;
    FILE *f = fopen(filename, "r");
    char line_buffer[512];

    if (NULL == f)
    {
        perror(filename);
        ret = 1;
        goto EXIT;
    }

    while (fgets(line_buffer, sizeof line_buffer, f))
    {
        if (0 == flm->nb_funcs)
        {
            if (1 == sscanf(line_buffer, "nb_func;%d\n", &flm->nb_funcs))
            {
                flm->funcs = calloc(flm->nb_funcs+1, sizeof *(flm->funcs));

                if (NULL == flm->funcs)
                {
                    perror("calloc");
                    exit(EXIT_FAILURE);
                }
            }
        }
        else
        {
            int idx, vidx;
            if (1 == sscanf(line_buffer, "fun[%d];", &idx))
            {
                if (idx < 0 || idx >= (flm->nb_funcs+1))
                    { continue; }
#define FUN flm->funcs[idx] 
#define FP &FUN.par
#define FPP &FUN.post    
#define FC &FUN.color.comp
                int r = sscanf(line_buffer,
                        "fun[%*d];%f;%f;%f;%f;%f;%f;%d;%f;%f;%f;%f;%f;%f;%f;%f;%f",
                        FP[0], FP[1], FP[2], FP[3], FP[4], FP[5],
                        &vidx,
                        FPP[0], FPP[1], FPP[2], FPP[3], FPP[4], FPP[5],
                        FC[0], FC[1], FC[2]);
                if (16 != r)
                {
                    perror("sscanf");
                }
                else if (vidx >=0 && vidx < variations_cnt)
                {
                    FUN.var = variations[vidx];                 
                }
                else
                {
                    fprintf(stderr, "error with variation of function %d\n", idx);
                    FUN.var = variations[0];
                }
#undef FUN  
            }
        }
        /* parse for symetry here */
        sscanf(line_buffer, "symetry;%d\n", &flm->symetry);
    }


    flm->final = flm->funcs[flm->nb_funcs];

EXIT:
    if (NULL != f)
        { fclose(f); }

    return ret;
}

void save_config_to_file(flame_config_t *flm, const char *filename)
{
    int i, j;
    FILE *f = fopen(filename, "w");

    if (NULL == f)
    {
        perror(filename);
        return;
    }

    /* nb functions */
    fprintf(f, "nb_func;%d\n", flm->nb_funcs);
    /* symetry */
    fprintf(f, "symetry;%d\n", flm->symetry);

    /* write fields info */
    /** @todo write this */

    /* list of functions */
    for (i = 0; i <= flm->nb_funcs; ++i)
    {
        fprintf(f, "fun[%d];", i);
        /* first transformation */
        for (j = 0; j < 6; ++j)
        {
            fprintf(f, "%f;", flm->funcs[i].par[j]);
        }

        /* variation */
        fprintf(f, "%d;", VAR_get_variation_address(flm->funcs[i].var));

        /* post transformation */
        for (j = 0; j < 6; ++j)
        {
            fprintf(f, "%f;", flm->funcs[i].post[j]);
        }

        /* color :*/
        for (j = 0; j < 3; ++j)
        {
            fprintf(f, "%f;", flm->funcs[i].color.comp[j]);
        }
        fprintf(f, "\n");
    }

    fclose(f);
}

flame_config_t* CFG_get_config(params_t* opts)
{
    flame_config_t *ret = calloc(1, sizeof *ret);
    if (NULL == opts->input)
    {
        /* generate function */
        ret->funcs = generate_random_functions(
            &opts->nb_functions, opts->tolerance, 0);

        ret->nb_funcs = opts->nb_functions;

        /*ret->vij = generate_v_matrix(ret->nb_funcs);*/

        /* remember final function params */
        ret->final = ret->funcs[ret->nb_funcs];

        /* symetry mode */
        ret->symetry = TOOLS_randi(0, 6);
    }
    else
    {
        read_config_from_file(ret, opts->input);
    }
    if (NULL != opts->output)
    {
        save_config_to_file(ret, opts->output);
    }

    return ret;
}

void CFG_free(flame_config_t* cfg)
{
    if (cfg)
    {
        free(cfg->funcs);
    }
    free(cfg);
}

void CFG_mutate(struct flame_config* flm)
{
    /* select function to mutate between 0 to flm->nb_funcs */
    
    /* change one parameter in transformation */
        /*    flm->funcs[i].par[j]); */
        /* change one parameter in post transformation */ 
    /* flm->funcs[i].post[j]); */

}
