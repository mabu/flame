/**
 *  @file options.h
 */
#ifndef OPTIONS_H
#define OPTIONS_H

#define _PARSE_COMMANDLINE_BEGIN(...)                       \
int parseCommandLine(int argc, char * argv[], __VA_ARGS__) {\
    int    error = 0, known = -1;                           \
    int    iLoop;                                           \
    for(iLoop = 1; iLoop < argc; ++iLoop) {                 \
        known = 0;

#define _PARSE_COMMANDLINE_END()                                        \
if(-1 == known)                                                         \
fprintf(stderr, "Error, option %s not recognized\n",argv[iLoop]);  }    \
return error; }

#define _DEFINE_OPTION(sOp, lOp, nbPar)                                     \
known = 0;                                                                  \
if(sOp == argv[iLoop][1])  {                                                \
    if(iLoop + 1 + nbPar > argc)         {                                  \
        fprintf(stderr, "Error, not enough params for -%c option\n", sOp);  \
        continue; }                                                         \
    known = 1;}                                                             \
if(!strncmp( "--"lOp , argv[iLoop], strlen(lOp) + 2)) {                     \
    if(iLoop + 1 + nbPar > argc) {                                          \
        fprintf(stderr, "Error, not enough params for --%s option\n", lOp); \
        continue; }                                                         \
    known = 1;}

#define _OPTION_SET_INTEGER(iVal)                           \
if(1 == known) {                                            \
    iVal = atoi(argv[++iLoop]);                             \
    continue;                                               \
}


#define _OPTION_SET_DOUBLE(dVal)                            \
if(1 == known) {                                            \
    dVal = atof(argv[++iLoop]);                             \
    continue;                                               \
}

#define _OPTION_SET_STRING(sVal)                            \
if(1 == known) {                                            \
    sVal = strdup(argv[++iLoop]);                           \
    continue;                                               \
}

#define _OPTION_ACTION(fAction) if(1 == known)  fAction();
#define _OPTION_SET_VALUE(var, val) if(1 == known)  (var) = (val);

#define TRACEx(x) printf(#x " = %x\n",x);
#define TRACEd(x) printf(#x " = %d\n",x);
#define TRACEf(x) printf(#x " = %lf\n",x);
#define TRACEs(x) printf(#x " = %s\n",x);

#endif
