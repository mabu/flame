#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <time.h>
#include "params.h"
#include "options.h"

_PARSE_COMMANDLINE_BEGIN(params_t *options)

_DEFINE_OPTION('i', "input", 1)
_OPTION_SET_STRING(options->input)
 
_DEFINE_OPTION('o', "output", 1)
_OPTION_SET_STRING(options->output)   

_DEFINE_OPTION('w', "width", 1)
_OPTION_SET_INTEGER(options->width)

_DEFINE_OPTION('h', "height", 1)
_OPTION_SET_INTEGER(options->height)

_DEFINE_OPTION('n', "functions", 1)
_OPTION_SET_INTEGER(options->nb_functions)

_DEFINE_OPTION('s', "seed", 1)
_OPTION_SET_INTEGER(options->seed)

_DEFINE_OPTION('t', "tol", 1)
_OPTION_SET_DOUBLE(options->tolerance)

_DEFINE_OPTION('N', "points", 1)
_OPTION_SET_INTEGER(options->nb_points)

_DEFINE_OPTION('v', "verbose", 0)
_OPTION_SET_VALUE(options->verbose, 1)

_DEFINE_OPTION('@', "disable-post", 0)
_OPTION_SET_VALUE(options->flags, options->flags | PARAM_FLAG_NO_POST_TRANSFORM)

_DEFINE_OPTION('@', "disable-final", 0)
_OPTION_SET_VALUE(options->flags, options->flags | PARAM_FLAG_NO_FINAL_TRANSFORM)

_PARSE_COMMANDLINE_END()

void usage(const char *name)
{
    fprintf(stderr, "usage:\t %s [OPTIONS] FILE\n\n", name);
    fprintf(stderr, "options are:\n");
    fprintf(stderr, "\t -w --width <int>: width of image, in pixels\n");
    fprintf(stderr, "\t -h --height <int>: height of image, in pixels\n");
    fprintf(stderr, "\t -i --input <file>: ");
    fprintf(stderr,     "read parameters instead of random generation\n");
    fprintf(stderr, "\t -o --output <file>: write generated parameters\n");
    fprintf(stderr, "\t -s --seed <int>: seed for srand\n");
    fprintf(stderr, "\t -n --functions <int>: number of functions\n");
    fprintf(stderr, "\t -N --points <int>: number of points to draw\n");
    fprintf(stderr, "\t --disable-post: disable post transform\n");
    fprintf(stderr, "\t --disable-final: disable final transform\n");
    fprintf(stderr, "\t -t --tol <float>: ");
    fprintf(stderr,     "tolerance for defining a contracting function\n");
    fprintf(stderr, "\t -v --verbose: verbose mode\n");
}

int OPT_parse(int argc, char *argv[], params_t *options)
{
    time_t now = time(NULL);
    /* set default parameters */
    options->seed = now;
    
    /* change seed */
    while (time(NULL) == now)
    {
        /* warning, this line could be optimized by some compiler */
        --options->seed;
    }
    
    if (0 != parseCommandLine(argc-1, argv, options))
    {
        usage(*argv);
        return 1;
    }
    
    if (argc > 1)
    {
        options->filename = strdup(argv[argc-1]);        
    }
    else
    {
        usage(*argv);
        return 1;
    }    
    
    srand(options->seed);
    
    return 0;
}

void OPT_free(params_t *options)
{
    free(options->input);
    free(options->output);
    free(options->filename);
}

void OPT_display(params_t *options)
{
    printf("File:         %s\n", options->filename);
    printf("geometry:     %dx%d\n", options->width, options->height);
    printf("nb_functions: %d\n", options->nb_functions);
    printf("nb_points:    %d\n", options->nb_points);
    printf("seed:         %d\n", options->seed);
    printf("tolerance:    %f\n", options->tolerance);
    printf("read from:    %s\n", options->input);
    printf("write to:     %s\n", options->output);
    printf("flags:        %d\n", options->flags);
    puts("-------------");
    
}
