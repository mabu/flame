#include <stdio.h>
#include <string.h>

#pragma pack (1)

typedef struct rgbq
{
    unsigned char m_blue; 
    unsigned char m_green; 
    unsigned char m_red; 
    unsigned char m_reserved; 
} RGBQ;

typedef struct bmfh
{ 
    unsigned short m_type; 
    unsigned int  m_size; 
    unsigned short m_reserved_1; 
    unsigned short m_reserved_2; 
    unsigned int  m_offset; 
} BMFH;


typedef struct bmih
{
    unsigned int  m_size; 
    int           m_width; 
    int           m_height; 
    unsigned short m_planes; 
    unsigned short m_bitCount; 
    unsigned int  m_compression; 
    unsigned int  m_sizeImage; 
    int           m_XPPM; 
    int           m_YPPM; 
    unsigned int  m_colorUsed; 
    unsigned int  m_colorImportant; 
} BMIH;


/** 
 * Write array of pixels in a file.
 * @param [in] pFilePath: zero terminated file path
 * @param [in] pPixels: array of pixel of depth bits.
 * @param [in] depth: 8 (gray), 24 or 32 (true colors)
 * @param [in] width: image width (should be multiple of 4)
 * @param [in] height: image height
 * @return -1 on error, 0 else. 
 */
int BMP_write(const char *filepath, void *pixels, 
                int depth, int width, int height)
{
    /* palette */
    RGBQ palette[256];
    /* file header */
    BMFH bmfh;
    /* bitmap header */
    BMIH bmih;

    /* check param */
    if (NULL == filepath || NULL == pixels || 0 == depth || 0 == width || 0 == height) 
    {
        return -1;
    }

    if (width%4)
    {
        fprintf(stderr, "width must be multiple of 4\n");
        return -1;
    }

    /* fill file header */
    memcpy( &bmfh.m_type, "BM", 2);
    bmfh.m_size = sizeof(bmfh) + sizeof(bmih) + depth / 8 * width * height;
    bmfh.m_reserved_1 = 0;
    bmfh.m_reserved_2 = 0;
    if (8 == depth) 
    {
        bmfh.m_offset = sizeof(bmfh) + sizeof(bmih) + 256 * sizeof(RGBQ);
    } 
    else 
    {
        bmfh.m_offset = sizeof(bmfh) + sizeof(bmih);
    }

    /* remplissage de l'entete de l'image */
    memset(&bmih, 0, sizeof bmih);
    bmih.m_size = sizeof(bmih);
    bmih.m_width = width;
    bmih.m_height = height;
    bmih.m_planes = 1;
    bmih.m_bitCount = (unsigned short)depth;
    bmih.m_compression = 0; /* no compression */
    bmih.m_sizeImage = 0;
    bmih.m_colorUsed = 0;
    bmih.m_colorImportant = 0;

    if (8 == depth) 
    {
        unsigned char i = 0;
        do 
        {
            palette[i].m_blue = palette[i].m_green = palette[i].m_red = i;            
        } while (++i);
    }

    {
        FILE *f = fopen(filepath, "wb");
        if (NULL == f)
        {
            perror(filepath);
        } 
        else 
        {
            /* write header */
            fwrite(&bmfh, sizeof bmfh, 1, f);
            fwrite(&bmih, sizeof bmih, 1, f);
      
            /* write palette */
            if (8 == depth) 
            {
                fwrite(palette, sizeof palette, 1, f);
            }
      
            fwrite(pixels, width * height, depth / 8, f);
            /* close file */
            fclose(f);
        }
    }
  
    return 0;

}

