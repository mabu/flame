#ifndef H_20110108_FUNCTIONS
#define H_20110108_FUNCTIONS

#include <stdlib.h> 

int BMP_write(const char *, void *, int, int, int);

struct params;
struct flame_config;

int OPT_parse(int, char *[], struct params *);
void OPT_display(struct params *);
void OPT_free(struct params *);

void FLAME_generate(float *[3], int, int, struct flame_config *);
void FLAME_render(unsigned char *, float *[3], int, int, int);
float ** FLAME_resize(float *[3], int, int);

struct flame_config* CFG_get_config(struct params*);
void CFG_free(struct flame_config*); 
void CFG_mutate(struct flame_config*); 

int VAR_get_variation_address(void*);

int TOOLS_randi(int, int);
float TOOLS_randf(float, float);


#endif /* guard */
