 /* main.c */


#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "params.h"
#include "functions.h"
#include "options.h"
#include "types.h"

#define DIE(what) perror(what); return EXIT_FAILURE;

#if HAVE_ZOOM_FACTOR
#define ZOOM_FACTOR 6
#endif

int main(int argc, char *argv[])
{
    int ret = EXIT_SUCCESS;
    /* structure to store parameters */
    params_t opts = DEFAULT_PARAMS;

    if (0 != OPT_parse(argc, argv, &opts))
    {
        ret = EXIT_FAILURE;
    }
    else
    {
        flame_config_t *flame_cfg = NULL;
        int i, side;
        unsigned char *image = NULL;
        float *data[3] = {NULL, NULL, NULL};
        float **data_small;
        /* compute fractal size from image size */
        side = opts.width;
        if (side < opts.height)
            { side = opts.height; }

        /* display parameters if verbose mode */
        if (opts.verbose)
            { OPT_display(&opts); }

        image = calloc(4 * opts.width * opts.height, sizeof *image);
        for (i = 0; i < 3; ++i)
        {
#if HAVE_ZOOM_FACTOR
            data[i] = calloc(ZOOM_FACTOR*side *ZOOM_FACTOR*side, sizeof **data);
#else
            data[i] = calloc(side *side, sizeof **data);
#endif
        }

        if (NULL == image)
            { DIE("malloc"); }

        /* compute fractal parameters from command line */
        flame_cfg = CFG_get_config(&opts);

        /* generate the fractal */
#if HAVE_ZOOM_FACTOR
        FLAME_generate(data, ZOOM_FACTOR*side, opts.nb_points, flame_cfg);

        /* resize the image (antialiasing) */
        data_small = FLAME_resize(data, ZOOM_FACTOR*side, side);

        /* make the fractal drawable */
        FLAME_render(image, data_small, opts.width, opts.height, side);
#else
        FLAME_generate(data, side, opts.nb_points, flame_cfg);

        /* make the fractal drawable */
        FLAME_render(image, data, opts.width, opts.height, side);

#endif
        /* write the image */
        BMP_write(opts.filename, image, 32, opts.width, opts.height);

        /* clean up memory */
        free(image);
    }

    /* free memory used by options (copied filenames) */
    OPT_free(&opts);
    return ret;
}
