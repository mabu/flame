#! /bin/bash

DIR=../public/images
LIST=$(pwd)/images.list

echo "creating image directory"
mkdir -p $DIR

for i in {1..24}
do
    echo "creating image $i"
    s=$RANDOM
    ../src/flame -w 512 -h 512 -s $s -N 100000000 $DIR/$s.bmp  
    convert -interlace Plane -quality 100 $DIR/$s.bmp $DIR/$s.jpg
    rm $DIR/$s.bmp
done

echo "images generated"

if [[ -f $LIST ]]
then
    rm $LIST
fi

echo "generating $LIST"
cd $DIR
for i in *jpg
    do echo \!\[seed: ${i%.jpg}\]\(images/$i \"seed: ${i%.jpg}\"\) >> $LIST
done 
