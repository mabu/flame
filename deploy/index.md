Flame
====

[`flame`](http://framagit.org/mabu/flame) is a simple and imcomplete implementation of [flame](http://flam3.com) algorithm.

Example
-------

With some luck, nice images can be generated:

<object width="100%" height="400%" type="text/html" data="images.list.html"></object>

Usage
-----

If you want to generate nice flam fractal, you should use a real software, not this one.

`flame [OPTIONS] filename`

`filename` is the file to generate. Only BMP format is supported 

Options are :

`-w, --width integer`: image width, in pixels, default is 256,

`-h, --height integer`: image height, in pixels, default is 256,

`-N integer`: number of points to compute, the more the nicer image,

`-s, --seed integer`: random number seed, default is based on time,

`-i, --input filename`: 

`-o, --output filename`:

`-v, --verbose`: verbose mode

Building
--------

Download sources and build with: 

    git clone http://framagit.org/mabu/flame
    cd flame
    autoreconf --install
    ./configure
    make
    make install

Will create the `flame` executable.


