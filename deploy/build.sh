#! /bin/sh

DIR=../public
DEST=$DIR/index.html
IMG=$DIR/images.list.html

./genimages.sh

cp header.html $DEST 
markdown index.md >> $DEST  
cat footer.html >> $DEST

markdown images.list > $IMG
